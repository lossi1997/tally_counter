
import logger from "./logger.mjs";
import app from "./router.mjs";

const PORT = 3000;
const HOSTNAME = "127.0.0.1";

const main = () => {
    logger.log("info", "Service starting.");
    app.listen(PORT, HOSTNAME, () => {
        logger.log("info", `Listening at http://${HOSTNAME}:${PORT}`);
    })
}

main();